#Description
* This image will start elasticsearch

#Build:
docker build -t artifact-es .

#Run :
docker run --rm -it -p 9200:9200/tcp -p 9300:9300/tcp artifact-es:latest