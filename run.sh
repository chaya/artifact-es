#!/bin/bash

# Add 2G Memory
ES_JAVA_OPTS="-Xms2g -Xmx2g ${ES_JAVA_OPTS}"

echo 'Initialization complete'
elasticsearch